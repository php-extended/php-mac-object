<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Mac;

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;

/**
 * MacAddressParser class file.
 *
 * Parser for 48 bits or 64 bits mac addresses.
 *
 * @author Anastaszor
 * @extends AbstractParserLexer<MacAddress48BitsInterface>
 */
class MacAddress48Parser extends AbstractParserLexer implements MacAddress48ParserInterface
{
	
	public const L_HEXA = 1;
	public const L_SCLN = 2;
	public const L_DUAL = 3;
	
	/**
	 * Builds a new MacAddressParser with it.
	 */
	public function __construct()
	{
		parent::__construct(MacAddress48BitsInterface::class);
		$this->_config->addMappings(LexerInterface::CLASS_HEXA, MacAddress48Parser::L_HEXA);
		$this->_config->addMappings(':', MacAddress48Parser::L_SCLN);
		$this->_config->addMerging(MacAddress48Parser::L_HEXA, MacAddress48Parser::L_HEXA, MacAddress48Parser::L_DUAL);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : MacAddress48BitsInterface
	{
		return parent::parse($data); // php interface compatibility
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\AbstractParserLexer::parseLexer()
	 */
	public function parseLexer(LexerInterface $lexer) : MacAddress48BitsInterface
	{
		$lexer->rewind();
		
		$byte1 = $this->expectOneOf($lexer, [self::L_DUAL]);
		$sep1 = $this->expectOneOf($lexer, [self::L_SCLN], $byte1);
		$byte2 = $this->expectOneOf($lexer, [self::L_DUAL], $sep1);
		$sep2 = $this->expectOneOf($lexer, [self::L_SCLN], $byte2);
		$byte3 = $this->expectOneOf($lexer, [self::L_DUAL], $sep2);
		$sep3 = $this->expectOneOf($lexer, [self::L_SCLN], $byte3);
		$byte4 = $this->expectOneOf($lexer, [self::L_DUAL], $sep3);
		$sep4 = $this->expectOneOf($lexer, [self::L_SCLN], $byte4);
		$byte5 = $this->expectOneOf($lexer, [self::L_DUAL], $sep4);
		$sep5 = $this->expectOneOf($lexer, [self::L_SCLN], $byte5);
		$byte6 = $this->expectOneOf($lexer, [self::L_DUAL], $sep5);
		
		$this->expectEof($lexer, $byte6);
		
		$oui = (((int) \hexdec($byte1->getData())) << 16)
			 + (((int) \hexdec($byte2->getData())) << 8)
			 + ((int) \hexdec($byte3->getData()));
		
		$nic = (((int) \hexdec($byte4->getData())) << 16)
			 + (((int) \hexdec($byte5->getData())) << 8)
			 + ((int) \hexdec($byte6->getData()));
		
		return new MacAddress48Bits($oui, $nic);
	}
	
}
