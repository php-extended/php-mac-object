<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Mac;

/**
 * MacAddress48Bits class file.
 *
 * This class represents a MAC (Media Access Control) address on 48 bits.
 *
 * @author Anastaszor
 */
class MacAddress48Bits implements MacAddress48BitsInterface
{
	
	/**
	 * Organisationally Unique Identifier. (24 bits).
	 *
	 * @var integer
	 */
	protected int $_oui = 0;
	
	/**
	 * Network Interface Controller (24 bits).
	 *
	 * @var integer
	 */
	protected int $_nic = 0;
	
	/**
	 * Builds an new MacAddress48Bits mac address from parsing given string or object.
	 *
	 * @param integer $oui the organisationally unique identifier
	 * @param integer $nic the network interface controller
	 */
	public function __construct(int $oui, int $nic)
	{
		$this->_oui = $oui & 0x00FFFFFF;
		$this->_nic = $nic & 0x00FFFFFF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->hexa2($this->getFirstByte())
			.':'.$this->hexa2($this->getSecondByte())
			.':'.$this->hexa2($this->getThirdByte())
			.':'.$this->hexa2($this->getFourthByte())
			.':'.$this->hexa2($this->getFifthByte())
			.':'.$this->hexa2($this->getSixthByte());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::getOui()
	 */
	public function getOui() : int
	{
		return $this->_oui;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::getNic()
	 */
	public function getNic() : int
	{
		return $this->_nic;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::getFirstByte()
	 */
	public function getFirstByte() : int
	{
		return ($this->_oui >> 16) & 0x000000FF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::getSecondByte()
	 */
	public function getSecondByte() : int
	{
		return ($this->_oui >> 8) & 0x000000FF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::getThirdByte()
	 */
	public function getThirdByte() : int
	{
		return $this->_oui & 0x000000FF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::getFourthByte()
	 */
	public function getFourthByte() : int
	{
		return ($this->_nic >> 16) & 0x000000FF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::getFifthByte()
	 */
	public function getFifthByte() : int
	{
		return ($this->_nic >> 8) & 0x000000FF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::getSixthByte()
	 */
	public function getSixthByte() : int
	{
		return $this->_nic & 0x000000FF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::isUnicast()
	 */
	public function isUnicast() : bool
	{
		return 0 === (($this->_nic >> 16) & 0x00000001);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::isMulticast()
	 */
	public function isMulticast() : bool
	{
		return 0x00000001 === (($this->_nic >> 16) & 0x00000001);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::isGloballyUnique()
	 */
	public function isGloballyUnique() : bool
	{
		return 0 === (($this->_nic >> 16) & 0x00000002);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::isLocallyUnique()
	 */
	public function isLocallyUnique() : bool
	{
		return 0x00000002 === (($this->_nic >> 16) & 0x00000002);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::isIpv4Multicast()
	 */
	public function isIpv4Multicast() : bool
	{
		return 0x0001005E === $this->_oui;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48BitsInterface::equals()
	 */
	public function equals($object) : bool
	{
		return ($object instanceof MacAddress48BitsInterface)
			&& $this->getNic() === $object->getNic()
			&& $this->getOui() === $object->getOui();
	}
	
	/**
	 * Returns a 2-byte hexadecimal string from an integer.
	 *
	 * @param integer $byte
	 * @return string
	 */
	protected function hexa2(int $byte) : string
	{
		return \str_pad(\dechex($byte), 2, '0', \STR_PAD_LEFT);
	}
	
}
