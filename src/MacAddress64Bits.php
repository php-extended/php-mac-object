<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Mac;

/**
 * MacAddress64Bits class file.
 * 
 * This class represents a MAC (Media Access Control) address on 64 bits.
 * 
 * @author Anastaszor
 */
class MacAddress64Bits extends MacAddress48Bits implements MacAddress64BitsInterface
{
	
	/**
	 * Organisationally Unique Identifier, additional bits (16 bits).
	 *
	 * @var integer
	 */
	protected int $_lnk = 0;
	
	/**
	 * Builds an new MacAddress64Bits mac address from parsing given string or object.
	 *
	 * @param int $oui the organisationally unique identifier
	 * @param int $lnk the additional bits of OUI
	 * @param int $nic the network interface controller
	 */
	public function __construct(int $oui, int $lnk, int $nic)
	{
		parent::__construct($oui, $nic);
		$this->_lnk = $lnk & 0x0000FFFF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->hexa2($this->getFirstByte())
			.':'.$this->hexa2($this->getSecondByte())
			.':'.$this->hexa2($this->getThirdByte())
			.':'.$this->hexa2($this->getFourthByte())
			.':'.$this->hexa2($this->getFifthByte())
			.':'.$this->hexa2($this->getSixthByte())
			.':'.$this->hexa2($this->getSeventhByte())
			.':'.$this->hexa2($this->getEighthByte());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress64BitsInterface::getLnk()
	 */
	public function getLnk() : int
	{
		return $this->_lnk;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48Bits::getFourthByte()
	 */
	public function getFourthByte() : int
	{
		return ($this->_lnk >> 8) & 0x000000FF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48Bits::getFifthByte()
	 */
	public function getFifthByte() : int
	{
		return $this->_lnk & 0x000000FF;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48Bits::getSixthByte()
	 */
	public function getSixthByte() : int
	{
		return parent::getFourthByte();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress64BitsInterface::getSeventhByte()
	 */
	public function getSeventhByte() : int
	{
		return parent::getFifthByte();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress64BitsInterface::getEighthByte()
	 */
	public function getEighthByte() : int
	{
		return parent::getSixthByte();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Mac\MacAddress48Bits::equals()
	 */
	public function equals($object) : bool
	{
		return ($object instanceof MacAddress64BitsInterface)
			&& $this->getNic() === $object->getNic()
			&& $this->getLnk() === $object->getLnk()
			&& $this->getOui() === $object->getOui();
	}
	
}
