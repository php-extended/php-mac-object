<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Mac;

use PhpExtended\Lexer\CodeFilterLexer;
use PhpExtended\Lexer\LexerConfiguration;
use PhpExtended\Lexer\LexerFactory;
use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Lexer\LexerLexer;
use PhpExtended\Parser\AbstractParser;
use PhpExtended\Parser\ParseException;

/**
 * MacAddressParser class file.
 *
 * Parser for 64 bits or 64 bits mac addresses.
 *
 * @author Anastaszor
 * @extends AbstractParser<MacAddress64BitsInterface>
 */
class MacAddress64Parser extends AbstractParser implements MacAddress64ParserInterface
{
	
	public const L_HEXA = 1;
	public const L_SCLN = 2;
	public const L_DUAL = 3;
	
	/**
	 * The lexer factory.
	 * 
	 * @var LexerFactory
	 */
	protected LexerFactory $_lexerFactory;
	
	/**
	 * The default config.
	 * 
	 * @var LexerConfiguration
	 */
	protected LexerConfiguration $_defaultConfig;
	
	/**
	 * Builds a new MacAddressParser with it.
	 */
	public function __construct()
	{
		$this->_lexerFactory = new LexerFactory();
		$this->_defaultConfig = new LexerConfiguration();
		$this->_defaultConfig->addMappings(LexerInterface::CLASS_HEXA, MacAddress64Parser::L_HEXA);
		$this->_defaultConfig->addMappings(':', MacAddress64Parser::L_SCLN);
		$this->_defaultConfig->addMerging(MacAddress64Parser::L_HEXA, MacAddress64Parser::L_HEXA, MacAddress64Parser::L_DUAL);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : MacAddress64BitsInterface
	{
		$data = (string) $data;
		$matches = [];
		if(\preg_match('#^(\\d+),(\\d+)$#', $data, $matches))
		{
			/** @phpstan-ignore-next-line */
			if(isset($matches[1], $matches[2]))
			{
				return new MacAddress64Bits((int) $matches[1], 0, (int) $matches[2]);
			}
		}
		
		$lexer = $this->createLexer($data);
		$parts = [];
		
		foreach($lexer as $lexeme)
		{
			/** @var \PhpExtended\Lexer\LexemeInterface $lexeme */
			if(MacAddress64Parser::L_DUAL === $lexeme->getCode())
			{
				$parts[] = $lexeme;
			}
			if(7 < \count($parts))
			{
				break;
			}
		}
		if(8 > \count($parts))
		{
			$message = 'Failed to find 8 hexadecimal places for a 64 bits mac address ({k} found).';
			$context = ['{k}' => \count($parts)];
			$endpart = \end($parts);
			$offset = (false === $endpart) ? 0 : $endpart->getColumn();
			
			throw new ParseException(MacAddress64BitsInterface::class, $data, $offset, \strtr($message, $context));
		}
		$oui = ((int) \hexdec($parts[0]->getData()) << 16) + ((int) \hexdec($parts[1]->getData()) << 8) + ((int) \hexdec($parts[2]->getData()));
		$lnk = ((int) \hexdec($parts[3]->getData()) << 8) + ((int) \hexdec($parts[4]->getData()));
		$nic = ((int) \hexdec($parts[5]->getData()) << 16) + ((int) \hexdec($parts[6]->getData()) << 8) + ((int) \hexdec($parts[7]->getData()));
		
		return new MacAddress64Bits($oui, $lnk, $nic);
	}
	
	/**
	 * Creates a new lexer from the factory and configuration.
	 * 
	 * @param string $string
	 * @return LexerInterface
	 */
	protected function createLexer(string $string) : LexerInterface
	{
		return new LexerLexer(new CodeFilterLexer(
			$this->_lexerFactory->createFromString($string, $this->_defaultConfig),
		), $this->_defaultConfig);
	}
	
}
