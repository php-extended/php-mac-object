# php-extended/php-mac-object

A library that implements the php-extended/php-mac-interface interface library.

![coverage](https://gitlab.com/php-extended/php-mac-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-mac-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-mac-object ^8`


## Basic Usage

You may use this library this following way:

```php

use PhpExtended\Mac\MacAddress48Bits;
use PhpExtended\Mac\ParseException;

$mac = new MacAddress48Bits(0xffffff, 0xffffff);

```

To parse the mac addresses, do :


```php

use PhpExtended\Mac\MacAddress48Parser;
use PhpExtended\Parser\ParseException;

$parser = new MacAddress48Parser();

$addr = '<put your mac address here>';	// "ff:ff:ff:ff:ff:ff" format

try
{
	$mac = $parser->parse($addr);
}
catch(ParseException $e)
{
	// does something
}

```

You can get back the first three bytes as integer with the `$mac->getOui()`
method and the three last bytes as integer with the `$mac->getNic()` method.


## License

MIT (See [license file](LICENSE)).
