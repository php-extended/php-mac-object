<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Mac\MacAddress64Bits;
use PHPUnit\Framework\TestCase;

/**
 * MacAddress64BitsTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Mac\MacAddress64Bits
 *
 * @internal
 *
 * @small
 */
class MacAddress64BitsTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MacAddress64Bits
	 */
	protected MacAddress64Bits $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('01:23:45:67:89:ab:cd:ef', $this->_object->__toString());
	}
	
	public function testGetLnk() : void
	{
		$this->assertEquals(0x6789, $this->_object->getLnk());
	}
	
	public function testGetFourthByte() : void
	{
		$this->assertEquals(0x67, $this->_object->getFourthByte());
	}
	
	public function testGetFifthByte() : void
	{
		$this->assertEquals(0x89, $this->_object->getFifthByte());
	}
	
	public function testGetSixthByte() : void
	{
		$this->assertEquals(0xAB, $this->_object->getSixthByte());
	}
	
	public function testGetSeventhByte() : void
	{
		$this->assertEquals(0xCD, $this->_object->getSeventhByte());
	}
	
	public function testGetEighthByte() : void
	{
		$this->assertEquals(0xEF, $this->_object->getEighthByte());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MacAddress64Bits(0x012345, 0x6789, 0xABCDEF);
	}
	
}
