<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Mac\MacAddress48Bits;
use PHPUnit\Framework\TestCase;

/**
 * MacAddress48BitsTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Mac\MacAddress48Bits
 *
 * @internal
 *
 * @small
 */
class MacAddress48BitsTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var MacAddress48Bits
	 */
	protected MacAddress48Bits $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('12:34:56:78:9a:bc', $this->_object->__toString());
	}
	
	public function testGetOui() : void
	{
		$this->assertEquals(0x123456, $this->_object->getOui());
	}
	
	public function testGetNic() : void
	{
		$this->assertEquals(0x789ABC, $this->_object->getNic());
	}
	
	public function testGetFirstByte() : void
	{
		$this->assertEquals(0x12, $this->_object->getFirstByte());
	}
	
	public function testGetSecondByte() : void
	{
		$this->assertEquals(0x34, $this->_object->getSecondByte());
	}
	
	public function testGetThirdByte() : void
	{
		$this->assertEquals(0x56, $this->_object->getThirdByte());
	}
	
	public function testGetFourthByte() : void
	{
		$this->assertEquals(0x78, $this->_object->getFourthByte());
	}
	
	public function testGetFifthByte() : void
	{
		$this->assertEquals(0x9A, $this->_object->getFifthByte());
	}
	
	public function testGetSixthByte() : void
	{
		$this->assertEquals(0xBC, $this->_object->getSixthByte());
	}
	
	public function testIsUnicast() : void
	{
		$this->assertTrue($this->_object->isUnicast());
	}
	
	public function testIsMulticast() : void
	{
		$this->assertFalse($this->_object->isMulticast());
	}
	
	public function testIsGloballyUnique() : void
	{
		$this->assertTrue($this->_object->isGloballyUnique());
	}
	
	public function testIsLocallyUnique() : void
	{
		$this->assertFalse($this->_object->isLocallyUnique());
	}
	
	public function testIsIpv4Multiast() : void
	{
		$this->assertFalse($this->_object->isIpv4Multicast());
	}
	
	public function testEquals() : void
	{
		$this->assertTrue($this->_object->equals($this->_object));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new MacAddress48Bits(0x123456, 0x789ABC);
	}
	
}
