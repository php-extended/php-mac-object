<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Mac\MacAddress64Bits;
use PhpExtended\Mac\MacAddress64Parser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * MacAddress64ParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Mac\MacAddress64Parser
 *
 * @internal
 *
 * @small
 */
class MacAddress64ParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var MacAddress64Parser
	 */
	protected MacAddress64Parser $_parser;
	
	public function testNull() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse(null);
	}
	
	public function testInts() : void
	{
		$this->assertEquals(new MacAddress64Bits(0, 0, 0), $this->_parser->parse('0,0'));
	}
	
	public function testZero() : void
	{
		$this->assertEquals(new MacAddress64Bits(0, 0, 0), $this->_parser->parse('00:00:00:00:00:00:00:00'));
	}
	
	public function testReal() : void
	{
		$this->assertEquals(new MacAddress64Bits(0x123456, 0x7890, 0xABCDEF), $this->_parser->parse('12:34:56:78:90:ab:cd:ef'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new MacAddress64Parser();
	}
	
}
