<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Mac\MacAddress48Bits;
use PhpExtended\Mac\MacAddress48Parser;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * MacAddress48ParserTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Mac\MacAddress48Parser
 *
 * @internal
 *
 * @small
 */
class MacAddress48ParserTest extends TestCase
{
	
	/**
	 * The parser to test.
	 * 
	 * @var MacAddress48Parser
	 */
	protected MacAddress48Parser $_parser;
	
	public function testNull() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_parser->parse(null);
	}
	
	public function testZero() : void
	{
		$this->assertEquals(new MacAddress48Bits(0, 0), $this->_parser->parse('00:00:00:00:00:00'));
	}
	
	public function testReal() : void
	{
		$this->assertEquals(new MacAddress48Bits(0x123456, 0x789ABC), $this->_parser->parse('12:34:56:78:9a:bc'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_parser = new MacAddress48Parser();
	}
	
}
